#!/usr/bin/awk -f
# Examine a maven-metadata.xml file and
# print the first found version of the snapshot artifact with
# the extension provided via -v extension=...
# where no classifier was set
/<classifier>/ {
  gsub(/<[^>]+>/, "")
  classifier = $1
}
$1 == "<extension>" extension "</extension>" {do_output=1}
/<value>/ {
  gsub(/<[^>]+>/, "");
  version = $1
}
/<\/snapshotVersion>/ {
  if (do_output && classifier == "" && version != "") {
    print version
    exit
  } else {
    do_output = 0
    classifier = ""
    version = ""
  }
}

