# Linux command line

## IP address

Read the address of the first non-loopback, non-docker interface
and put the result into the `HOST_IP` variable

```
HOST_IP=$(ip -brief address | awk '!/^(lo|docker)/ { $0 = $3; sub("/.*", ""); print; exit }')
```

The `ip` command can be shortened to `ip -br a`.

